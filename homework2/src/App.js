import React, { useState } from "react";
import { Component } from "react/cjs/react.production.min";
import './App.css';

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      items:[],
      loading:false
    }
  }

  componentDidMount(){
    fetch("https://randomuser.me/api/")
    .then((response) => response.json())
    .then((response) => {
      this.setState({
        items:response.results,
        loading:true
      })
    })
  }

  render(){
    var {items,loading} = this.state
    return(
          <div className="container">
            {items.map(item =>(
              <div>
                <div>
                  <img src={item.picture.large} alt={item.name.first} />
                  <p>email: {item.email}</p>
                  <p>gender: {item.gender}</p>
                  <p>name: {item.name.title} {item.name.first} {item.name.last}</p>
                  
                </div>
                <button onClick={item.picture.large}> Generate User </button>
              </div>
              
              
            ))}
            </div>
    )
  }
}
export default App
