import React from "react";
import './Style.css';


function PictureCard(props) {
    console.log(props);
      return (
          <div className="card">
          <img src={props.pictureData.imgSrc} alt="image" width = {200} height = {150}/>
          <div className="container">
            <h4><b>Date:{props.pictureData.date}</b></h4>
            <p>Like:{props.pictureData.likeCount}</p>
            <p>Comment:{props.pictureData.commentCount}</p>
            <p>Create By:{props.pictureData.createBy}</p>
          </div>
        </div>
      );
  }
  
export default PictureCard