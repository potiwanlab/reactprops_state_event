import logo from './logo.svg';
import './App.css';
import Navbar from './Component/Navbar'
import Footer from './Component/Footer';
import PictureCard from './Component/PictureCard';
const row = [0, 1, 2, 3, 4]

const pictureData = {id: 1,
  imgSrc: 'https://ichef.bbci.co.uk/news/800/cpsprodpb/1124F/production/_119932207_indifferentcatgettyimages.png.webp',
  createBy: "potiwan",
  date:"31/12/2021",
  likeCount: 123,
  commentCount: 456,
 }
function App() {
  return (
    <div>
      <Navbar />
      <div className="row">
        {row.map((c,i) => (
          <div className="col-md-3">
            <PictureCard pictureData={pictureData} key={i}/>
          </div>
        ))
        }
      </div>
      <Footer/>
    </div>
  );

}


export default App;
